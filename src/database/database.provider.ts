import { DataSource, DataSourceOptions } from 'typeorm';
import { DEFAULT_DATASOURCE } from './constants';
import { User } from '../v1/user/entities/user.entity';

export interface DatabaseConnectionOptions {
  default: DataSourceOptions;
}

export const databaseProviders = [
  {
    provide: DEFAULT_DATASOURCE,
    useFactory: async () => {
      const dataSource = new DataSource({
        name: 'default',
        type: 'postgres',
        host: process.env.DB_HOST,
        port: +process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        schema: process.env.DB_SCHEMA,
        entities: [User],
        synchronize: true,
      });
      return dataSource.initialize();
    },
  },
];
