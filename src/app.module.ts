import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { UserModuleV2 } from './v2/user/user.module';
import { UserModule } from './v1/user/user.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule.forRoot(), UserModule, UserModuleV2, DatabaseModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
