import { DeleteResult } from 'typeorm';
import { CommonEntity } from '../entities/common.entity';
import { CommonRepository } from '../repositories/common.repository';

export class RemoveMixin<T extends CommonEntity> {
  repository: CommonRepository<T>;

  removeById(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }
}

export interface Remove<T> {
  removeById(id: number): Promise<DeleteResult>;
}
