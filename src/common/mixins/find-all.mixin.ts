import { CommonEntity } from '../entities/common.entity';
import { CommonRepository } from '../repositories/common.repository';
import { Page, PaginatedQueryOptions } from '../types/interfaces';

export class FindAllMixin<T extends CommonEntity> {
  repository: CommonRepository<T>;

  findAll(options?: PaginatedQueryOptions): Promise<Page<T>> {
    options = { page: 1, rpp: 10, ...options };
    return this.repository.findPaginated(options.page, options.rpp, options);
  }
}

export interface FindAll<T> {
  findAll(): Promise<T[]>;
}
