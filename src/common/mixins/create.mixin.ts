import { DeepPartial } from 'typeorm';
import { CommonEntity } from '../entities/common.entity';
import { CommonRepository } from '../repositories/common.repository';

export class CreateMixin<T extends CommonEntity> {
  repository: CommonRepository<T>;

  create(data: DeepPartial<T>): Promise<T> {
    const entity = this.repository.create(data);
    return this.repository.save(entity);
  }
}

export interface Create<T> {
  create(dto: DeepPartial<T>): Promise<T>;
}
