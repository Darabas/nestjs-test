import { CommonEntity } from '../entities/common.entity';
import { CommonRepository } from '../repositories/common.repository';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export class UpdateMixin<T extends CommonEntity> {
  repository: CommonRepository<T>;

  updateById(id: number, dto: QueryDeepPartialEntity<T>): Promise<T> {
    return this.repository.updateById(id, dto);
  }
}

export interface Update<T> {
  updateById(id: number, dto: QueryDeepPartialEntity<T>): Promise<T>;
}
