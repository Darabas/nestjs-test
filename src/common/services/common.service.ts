import { DeepPartial, DeleteResult } from 'typeorm';
import { CommonEntity } from '../entities/common.entity';
import { CommonRepository } from '../repositories/common.repository';
import { Page, PaginatedQueryOptions } from '../types/interfaces';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export abstract class CommonService<T extends CommonEntity> {
  repository: CommonRepository<T>;

  constructor(repository: CommonRepository<T>) {
    this.repository = repository;
  }

  findAll(options?: PaginatedQueryOptions): Promise<Page<T>> {
    options = { page: 1, rpp: 10, ...options };
    return this.repository.findPaginated(options.page, options.rpp, options);
  }

  create(data: DeepPartial<T>): Promise<T> {
    const entity = this.repository.create(data);
    return this.repository.save(entity);
  }

  updateById(id: number, dto: QueryDeepPartialEntity<T>): Promise<T> {
    return this.repository.updateById(id, dto);
  }

  removeById(id: number): Promise<DeleteResult> {
    return this.repository.delete(id);
  }
}
