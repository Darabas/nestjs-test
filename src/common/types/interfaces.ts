import { FindManyOptions } from 'typeorm';

export interface Page<T> {
    page: number;
    rpp: number;
    totalCount: number;
    list: Array<T>;
}

export interface PaginatedQueryOptions extends FindManyOptions {
    page: number;
    rpp: number;
}
