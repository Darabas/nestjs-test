import {
  FindManyOptions,
  Repository,
} from 'typeorm';
import { CommonEntity } from '../entities/common.entity';
import { Page } from '../types/interfaces';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export class CommonRepository<T extends CommonEntity> extends Repository<T> {
  async findPaginated(
    page: number,
    rpp: number,
    options?: FindManyOptions,
  ): Promise<Page<T>> {
    const findAndCountOptions: FindManyOptions = {
      take: rpp,
      skip: (page - 1) * rpp,
      ...options,
    };

    const [list, totalCount] = await this.findAndCount(findAndCountOptions);

    const result: Page<T> = {
      page,
      rpp,
      list,
      totalCount,
    };

    return result;
  }

  async updateById(
    id: number,
    partialEntity: QueryDeepPartialEntity<T>,
  ): Promise<T | undefined> {
    const updateResult = await this.createQueryBuilder()
      .update()
      .set(partialEntity)
      .where({ id })
      .returning('*')
      .execute();

    return updateResult.raw[0];
  }
}
