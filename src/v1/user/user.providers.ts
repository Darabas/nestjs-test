import { DataSource } from 'typeorm';
import { DEFAULT_DATASOURCE } from '../../database/constants';
import { UserRepository } from './repositories/user.repository';
import { Provider } from '@nestjs/common';
import { UserService } from './services/user.service';
import { User } from './entities/user.entity';

export const USER_REPOSITORY = 'USER_REPOSITORY'
export const USER_SERVICE = 'USER_SERVICE'

export const userRepository: Provider = {
    provide: USER_REPOSITORY,
    useFactory: (dataSource: DataSource) => {
        return new UserRepository(User, dataSource.createEntityManager())
    },
    inject: [DEFAULT_DATASOURCE],
}

export const userService: Provider = {
    provide: USER_SERVICE,
    useFactory: (userRepository: UserRepository) => {
        return new UserService(userRepository)
    },
    inject: [USER_REPOSITORY],
}