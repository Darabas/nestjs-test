import { CommonRepository } from '../../../common/repositories/common.repository';
import { User } from '../entities/user.entity';

export class UserRepository extends CommonRepository<User> {}
