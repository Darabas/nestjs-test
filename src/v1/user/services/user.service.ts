import { Injectable } from '@nestjs/common';
import { CommonService } from 'src/common/services/common.service';
import { User } from '../entities/user.entity';

@Injectable()
export class UserService extends CommonService<User> {
}
