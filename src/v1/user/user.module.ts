import { Module } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { userRepository, userService } from './user.providers';

@Module({
  controllers: [UserController],
  providers: [userRepository, userService],

})
export class UserModule {}
