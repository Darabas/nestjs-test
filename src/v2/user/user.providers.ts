import { DataSource } from 'typeorm';
import { DEFAULT_DATASOURCE } from '../../database/constants';
import { UserRepository } from './repositories/user.repository';
import { Provider } from '@nestjs/common';
import { User } from 'src/v1/user/entities/user.entity';

export const USER_REPOSITORY = 'USER_REPOSITORY';

export const userRepository: Provider = {
  provide: USER_REPOSITORY,
  useFactory: (dataSource: DataSource) => {
    return new UserRepository(User, dataSource.createEntityManager());
  },
  inject: [DEFAULT_DATASOURCE],
};
