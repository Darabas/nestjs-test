import { Module } from '@nestjs/common';
import { UsersService } from './services/user.service';
import { UserController } from './controllers/user.controller';
import { userRepository } from './user.providers';

@Module({
  controllers: [UserController],
  providers: [UsersService, userRepository]
})
export class UserModuleV2 {}
