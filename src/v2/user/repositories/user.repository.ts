import { User } from 'src/v1/user/entities/user.entity';
import { CommonRepository } from '../../../common/repositories/common.repository';

export class UserRepository extends CommonRepository<User> {}
