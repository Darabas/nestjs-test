import { Inject, Injectable } from '@nestjs/common';
import { FindAll, FindAllMixin } from 'src/common/mixins/find-all.mixin';
import { Create, CreateMixin } from 'src/common/mixins/create.mixin';
import { applyMixins } from 'src/common/mixins/apply';
import { USER_REPOSITORY } from '../user.providers';
import { UserRepository } from '../repositories/user.repository';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-tipos-animai.dto';
import { Update, UpdateMixin } from 'src/common/mixins/update.mixin';
import { DeleteResult } from 'typeorm';
import { Remove, RemoveMixin } from 'src/common/mixins/remove.mixin';
import { User } from 'src/v1/user/entities/user.entity';

@Injectable()
export class UsersService implements FindAll<User>, Create<User>, Update<User>, Remove<User> {
  constructor(
    @Inject(USER_REPOSITORY)
    private readonly repository: UserRepository,
  ) {}

  create: (createDto: CreateUserDto) => Promise<User>;
  
  updateById: (id: number, updateDto: UpdateUserDto) => Promise<User>;

  findAll: () => Promise<User[]>;

  removeById: (id: number) => Promise<DeleteResult>
}

applyMixins(UsersService, [FindAllMixin, CreateMixin, UpdateMixin, RemoveMixin]);
